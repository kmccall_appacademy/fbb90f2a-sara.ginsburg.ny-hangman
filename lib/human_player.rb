require_relative 'hangman'

class HumanPlayer
  attr_reader :indices

  def guess(board)

    print "> "
    gets.chomp
  end

  def pick_secret_word
    print "what is the length of your secret word? "
    @length = gets.chomp.to_i
  end

  def register_secret_length(length)
    puts "the length of the secret word is #{length}"
  end

  def check_guess(letter)  #returns the indices of the found letters in the secret word
    indices = []
    print_board(board)
    puts
    puts "enter positions of #{letter}, "
    puts "(example: for 'l' in 'hello', enter 3,4)"
    puts "if word does not contain the letter, enter 0"
    pos = gets.chomp
    if pos.to_i != 0
      indices = pos.split(",").map {|idx| idx.to_i - 1}
    end
    indices
  end

  def handle_response(letter, indices)
    if indices.empty?
      puts "#{letter} is not found in this word"
    else
      puts "found #{letter} at position #{indices}"
    end
  end

  private


  def print_board(board)
    board_string = @board.map {|ch| ch.nil? ? "_" : ch}.join(" ")
    puts board_string
  end

end
