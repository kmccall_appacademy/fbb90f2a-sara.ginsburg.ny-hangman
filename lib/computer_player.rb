require_relative 'hangman'
require 'byebug'

class ComputerPlayer
  attr_reader :indices

  def self.read_dictionary
    File.readlines('lib/dictionary.txt').map(&:chomp)
  end

  attr_reader   :candidate_words, :board

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
      @secret_word = @dictionary.sample
      @length = @secret_word.length
  end

  def check_guess(letter)  #returns the indices of the found letters in the secret word
    indices = []
    @secret_word.chars.each_with_index {|char,i| indices << i if char == letter}
    indices
  end

  def register_secret_length(length)
      @candidate_words = @dictionary.select {|word| word.length == length}
      puts @candidate_words
      puts board
  end

  def handle_response(letter, indices)
    if indices != []
      @candidate_words.reject! do |word|
        (0...word.length).find_all {|i| word[i] == letter}.sort != indices.sort
      end
    else
      @candidate_words.reject! {|word| word.include?(letter)}
    end
  end

  def letter_indices(letter)
    indices = []
    @secret_word.chars.each_with_index {|char, i| indices << i if char == letter}
    indices
  end

  def guess(board)
    letter = most_common_letter(board)
    print letter
    letter
  end

  def most_common_letter(board)
    counts = Hash.new(0)
    letters = letters_for_empty_pos(board)
    letters.each {|l| counts[l] += 1}
    counts.key(counts.values.max)
  end

  def letters_for_empty_pos(board)
    letters = []
    @candidate_words.each do |word|
      word.chars.each_with_index do |ch, i|
        letters << ch if board[i].nil?
      end

    end
    letters
  end
end

if __FILE__ == $PROGRAM_NAME
  board = @board
  dictionary = ComputerPlayer.read_dictionary
  c = ComputerPlayer.new(dictionary)
  c.candidate_words
  c.guess(board)
  c.most_common_letter

end
