require_relative "computer_player"
require_relative "human_player"
require 'byebug'

class Hangman
  attr_reader :guesser, :referee, :board, :indices

  def initialize(players = [:guesser, :referee])
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    length = @referee.pick_secret_word
    @board = Array.new(length)
    @guesser.register_secret_length(length)

  end

  def take_turn
    letter = @guesser.guess(board)
    # debugger
    @indices = @referee.check_guess(letter)
    # debugger
    update_board(indices, letter) if indices != []
    # debugger
    print_board(board)
    @guesser.handle_response(letter, indices)
  end

  def update_board(indices, letter)
    indices.each {|idx| @board[idx] = letter}
    @board
  end

  def print_board(board)
    board_string = board.map {|el| el.nil? ? "_": el}.join(" ")
    puts board_string
  end

  def over?
    @board.count(nil) == 0
  end

  def conclude
    puts "Congratulations, you figured it out! the word was #{@board.join('')}"
  end
  # def guesser
  # end
end

if __FILE__ == $PROGRAM_NAME
  dictionary = ComputerPlayer.read_dictionary
  players = {referee: ComputerPlayer.new(dictionary),
             guesser: HumanPlayer.new}
  game = Hangman.new(players)

  game.play

end
